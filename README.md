# KavApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### Reference
* ##### changing paget display: [Angular Material Data Table Tutorial on second 168](https://youtu.be/ao-nY-9biWs?t=163)
* ##### Using server side data for mat-table: [connect-datasource-from-external-api-in-angular-material2](https://www.wikitechy.com/tutorials/angular-material/connect-datasource-from-external-api-in-angular-material2)

### TODO
* ##### Sorting: 
    * [Angular Material Tutorial - 30 - Data table Sorting](https://www.youtube.com/watch?v=AaRBjv5Thx0)
    * [Angular 5 Material Table with Sortable Headers, at 1183 seconds](https://youtu.be/NSt9CI3BXv4?t=1183)
* ##### Adding action column, Loading icon
    * [Angular Material Data Table - Paging, Sorting and Filter Operation, at 792 seconds](https://youtu.be/7wilnsiotqM?t=792)


This project depends on the Backend from the below project:
https://gitlab.com/public_projects/kav-cassandra-helper.git
https://gitlab.com/public_projects/kav-file-format-changer (specifically kav-firebase-service)
