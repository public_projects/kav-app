export interface CdrElement {
  no?: string;
  id?: string;
  callDate: string;
  callDuration: number;
  partyATarget: string;
  partyALocation: string;
  partyBTarget: string;
  partyBLocation: string;
}
