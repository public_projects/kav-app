export interface AreaModel {
  name: string;
  cgi: string;
  lat: string;
  long: string;
}
