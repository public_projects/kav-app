import {TargetDetailsModel} from './TargetDetailsModel';
import {AreaModel} from './AreaModel';
import {SourceModel} from './SourceModel';

export interface CallActivityModel {
  mcInterceptId: string;
  partyA: TargetDetailsModel;
  partyACgi: AreaModel;
  partyB: TargetDetailsModel;
  partyBCgi: AreaModel;
  startTime: string;
  source: SourceModel;
  contentLocator: string;
  smsMsg: string;
  duration: number;
}
