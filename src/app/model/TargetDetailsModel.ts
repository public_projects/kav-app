export interface TargetDetailsModel {
  name: string;
  mcngId: string;
  msisdn: string;
  imsi: string;
  imei: string;
}
