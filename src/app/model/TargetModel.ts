export interface TargetModel {
  mcngId: string;
  name: string;
  IMEI: string[];
  MSISDN: string[];
  IMSI: string[];
}
