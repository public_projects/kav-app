export interface SourceModel {
  // "mcng"
  system: string;
  // "3"
  agencyId: string;
  // "AG03"
  agencyName: string;
}
