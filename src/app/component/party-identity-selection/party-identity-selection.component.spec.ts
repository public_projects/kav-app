import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartyIdentitySelectionComponent } from './party-identity-selection.component';

describe('PartyIdentitySelectionComponent', () => {
  let component: PartyIdentitySelectionComponent;
  let fixture: ComponentFixture<PartyIdentitySelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartyIdentitySelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartyIdentitySelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
