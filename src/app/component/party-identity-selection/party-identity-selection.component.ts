import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {TargetModel} from '../../model/TargetModel';
import {MatRadioChange} from '@angular/material/radio';
import {TargetDetailsModel} from '../../model/TargetDetailsModel';

@Component({
  selector: 'app-party-identity-selection',
  templateUrl: './party-identity-selection.component.html',
  styleUrls: ['./party-identity-selection.component.css']
})
export class PartyIdentitySelectionComponent implements OnInit, OnChanges {
  hidden: boolean;
  msisdnList: string[];
  imsiList: string[];
  imeiList: string[];
  manualSelect: boolean;
  mainSelection: boolean;
  selectedMsisdn: string = null;
  selectedImei: string = null;
  selectedImsi: string = null;
  selectedName: string;
  partyData: TargetModel;
  @Output() newPartySelectionEvent = new EventEmitter<TargetDetailsModel>();

  constructor() {
    this.hidden = true;
  }

  ngOnInit(): void {
  }

  @Input()
  set selectedPartyData(value: TargetModel) {
    if (value !== null && value !== undefined) {
      this.partyData = value;
      this.hidden = false;
      this.msisdnList = value.MSISDN;
      this.imsiList = value.IMSI;
      this.imeiList = value.IMEI;

      console.log('new [MSISDN]' + value.MSISDN);
      console.log('new [IMEI]' + value.IMEI);
      console.log('new [IMSI]' + value.IMSI);
      this.selectedName = value.name;
      this.selectedMsisdn = this.msisdnList[0];
      this.mainSelection = true;
      this.manualSelect = false;
    }
  }

  msisdnDefaultChecked(i: number) {
    if (i === 0) {
      return true;
    }
    return false;
  }

  radioChange($event: MatRadioChange) {
    console.log($event.source.name, $event.value);

    if ($event.value === '3') {
      this.mainSelection = false;
      console.log('', $event.value);
      this.manualSelect = true;
    } else {
      this.manualSelect = false;
    }
  }

  radioChangeIdentity($event: MatRadioChange, identityType: string) {
    console.log($event.source.name, $event.value, identityType);
    const selectedValue = $event.value;
    this.changeIdentity(selectedValue, identityType);
  }

  changeIdentity(selectedValue: string, identityType: string) {
    let value = '-';
    if (selectedValue !== 'NONE') {
      value = selectedValue;
    }

    if (identityType === 'MSISDN') {
      this.selectedMsisdn = value;
    } else if (identityType === 'IMEI') {
      this.selectedImei = value;
    } else {
      this.selectedImsi = value;
    }

    if (this.selectedMsisdn !== null && this.selectedImei !== null && this.selectedImsi !== null) {
      const modal: TargetDetailsModel = {
        name: this.selectedName,
        msisdn: this.selectedMsisdn,
        imei: this.selectedImei,
        imsi: this.selectedImsi,
        mcngId: null
      };
      this.newPartySelectionEvent.emit(modal);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['selectedPartyData'].currentValue === null) {
      this.hidden = true;
      this.newPartySelectionEvent.emit(null);

      this.selectedMsisdn = null;
      this.selectedImei = null;
      this.selectedImsi = null;
    }
  }
}
