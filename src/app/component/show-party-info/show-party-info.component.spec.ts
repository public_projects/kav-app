import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPartyInfoComponent } from './show-party-info.component';

describe('ShowPartyInfoComponent', () => {
  let component: ShowPartyInfoComponent;
  let fixture: ComponentFixture<ShowPartyInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPartyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPartyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
