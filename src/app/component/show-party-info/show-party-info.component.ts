import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-show-party-info',
  templateUrl: './show-party-info.component.html',
  styleUrls: ['./show-party-info.component.css']
})
export class ShowPartyInfoComponent implements OnInit {
  partyInfo: string = null;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.partyInfo = JSON.stringify(this.data.selectedElement, null, '\t');
  }
}
