import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CgiActivityFormArrayComponent } from './cgi-activity-form-array.component';

describe('CgiActivityFormArrayComponent', () => {
  let component: CgiActivityFormArrayComponent;
  let fixture: ComponentFixture<CgiActivityFormArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CgiActivityFormArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CgiActivityFormArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
