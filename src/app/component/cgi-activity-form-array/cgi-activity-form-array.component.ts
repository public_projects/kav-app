import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {DataModel1} from '../../model/DataModel1';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-cgi-activity-form-array',
  templateUrl: './cgi-activity-form-array.component.html',
  styleUrls: ['./cgi-activity-form-array.component.css']
})
export class CgiActivityFormArrayComponent implements OnInit {
  title = 'kav-app';
  cgiActivityFormGroup: FormGroup;
  myGroup: FormGroup;

  constructor(private http: HttpClient, private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.getData();

    this.cgiActivityFormGroup = this.fb.group({
      activityLs: this.fb.array([])
    });

    this.myGroup = new FormGroup({
      firstName: new FormControl()
    });
  }
  get activityLs(): FormArray {
    return this.cgiActivityFormGroup.get('activityLs') as FormArray;
  }

  /**
   * Getting data from https://gitlab.com/public_projects/kav-cassandra-helper.git
   */
  getData() {
    this.http
        .get<{ [key: string]: DataModel1 }>('http://localhost:9011/msisdn/activities')
        .pipe(map(responseData => {
              const dataModel1Array: DataModel1[] = [];
              for (const key in responseData) {
                if (responseData.hasOwnProperty(key)) {
                  dataModel1Array.push({...responseData[key], id: key});
                }
              }
              return dataModel1Array;
            }
        ))
        .subscribe(responseData => {
          // console.log(responseData);
          this.createFormArrayForCgiActivities(responseData);
        });
  }

  createFormArrayForCgiActivities(cgiActivities: DataModel1[]): FormArray {
    const cgiActivitiesFormArray = new FormArray([]);
    console.log('cgiActivities length: ' + cgiActivities.length);
    for (let cgiActivity of cgiActivities) {
      this.activityLs.push(
          new FormGroup({
            'cgi': new FormControl(cgiActivity.key),
            'datebucket': new FormControl(cgiActivity.key2)
          })
      );
    }
    console.log('', cgiActivitiesFormArray);
    return cgiActivitiesFormArray;
  }

}
