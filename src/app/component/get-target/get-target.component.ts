import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TargetService} from '../../service/target.service';
import {TargetModel} from '../../model/TargetModel';
import {AreaService} from '../../service/area.service';
import {AreaModel} from '../../model/AreaModel';
import {DatePipe} from '@angular/common';
import {GeneratorService} from '../../service/generator.service';
import {DomSanitizer} from '@angular/platform-browser';
import {CdrModel} from '../../model/CdrModel';
import {AddAreaComponent} from '../add-area/add-area.component';
import {MatDialog} from '@angular/material/dialog';
import {CdrElement} from '../../model/CdrElement';
import {Observable, Subscription} from 'rxjs';
import {GenerateMcngConfigDialogComponent} from '../generate-mcng-config-dialog/generate-mcng-config-dialog.component';
import {FaunaAdminClientService} from '../../service/fauna-admin-client.service';
import {GenerateMcngEtlService} from '../../service/generate-mcng-etl.service';
import {TargetDetailsModel} from '../../model/TargetDetailsModel';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-get-target',
  templateUrl: './get-target.component.html',
  styleUrls: ['./get-target.component.css']
})
export class GetTargetComponent implements OnInit, OnDestroy {
  fileUrl;
  cdrCreatorForm: FormGroup;
  loadedTargets: TargetModel[];
  loadedTargetANames: string[] = [];
  loadedAreas: AreaModel[];
  records: CdrModel[];
  displayedColumns: string[] = [];
  // dataSource: MatTableDataSource<CdrElement[]>;
  dataSource: CdrElement[];
  partyAData: TargetModel = null;
  partyBData: TargetModel = null;
  partyALocation: AreaModel = null;
  partyBLocation: AreaModel = null;
  displayDateFormat = 'dd.MM.yyyy HH:mm:ss';
  areaModelSubscription: Subscription;
  targetModelSubscription: Subscription;
  targetModelSubjectSubscription: Subscription;
  showSpinner = true;
  @ViewChild('jsonfileInput')
  jsonfileInputVariable: ElementRef;
  filteredAOptions: Observable<string[]>;

  constructor(private targetService: TargetService,
              private areaService: AreaService,
              private datePipe: DatePipe,
              private generatorService: GeneratorService,
              private sanitizer: DomSanitizer,
              public dialog: MatDialog,
              private faunaAdminClientService: FaunaAdminClientService,
              private generateMcngEtlService: GenerateMcngEtlService) {
  }

  ngOnInit(): void {
    this.targetModelSubjectSubscription = this.targetService.targetSubject.subscribe(targetModels => {
      this.loadedTargets = targetModels;
      for (const val of this.loadedTargets) {
        this.loadedTargetANames.push(val.name);
      }

      // console.log('Subscribed to Target with loaded target:', this.loadedTargets);
      // console.log('this.loadedAreas is loaded?', this.loadedAreas);
      if (this.loadedAreas !== null && this.loadedAreas !== undefined) {
        this.showSpinner = false;
        // console.log('[targetModelSubjectSubscription]spinner: ', this.showSpinner);
      }
    });

    this.areaModelSubscription = this.areaService.areaModelSubject.subscribe(areaModels => {
      this.loadedAreas = areaModels;
      console.log('Subscribed to Area with loaded area:', this.loadedAreas);
      // console.log('this.loadedTargets is loaded?', this.loadedTargets);

      if (this.loadedTargets !== null && this.loadedTargets !== undefined) {
        this.showSpinner = false;
        // console.log('[areaModelSubscription]spinner: ', this.showSpinner);
      }
    });

    // console.log('1. Load area!');
    this.areaService.getAreas().subscribe(areaModels => {
        this.loadedAreas = areaModels;
        // console.log('[fethed][area][value]', this.loadedAreas);
      }
    );

    this.records = [];

    this.cdrCreatorForm = new FormGroup({
      callDate: new FormControl(null, [Validators.required]),
      callDuration: new FormControl(null, Validators.required),
      partyATarget: new FormControl(null, Validators.required),
      partyATargetIndentities: new FormGroup({
        msisdn: new FormControl(null),
        imei: new FormControl(null),
        imsi: new FormControl(null)
      }),
      partyBTargetIndentities: new FormGroup({
        msisdn: new FormControl(null),
        imei: new FormControl(null),
        imsi: new FormControl(null)
      }),
      partyBTarget: new FormControl(null),
      partyALocation: new FormControl(null),
      partyBLocation: new FormControl(null),
      cgiA: new FormControl(null),
      cgiB: new FormControl(null),
      coordinateA: new FormGroup({
        longitude: new FormControl(null),
        latitude: new FormControl(null)
      }, Validators.required),
      coordinateB: new FormGroup({
        longitude: new FormControl(null),
        latitude: new FormControl(null)
      })
    });

    console.log('1. Load target!');
    this.targetModelSubscription = this.targetService.getTargets().subscribe(targetModels => {
      this.loadedTargets = targetModels;
      console.log('Loaded targets;', this.loadedTargets);
    });

    // this.subscriptionArea = this.areaService.areaSubscriber$.subscribe(areaModels => {
    //   this.loadedAreas = areaModels;
    //   console.log('Subscribed to Area with loaded area:', this.loadedAreas);
    // });

    this.displayedColumns = ['num', 'callDate', 'callDuration', 'partyATarget', 'partyALocation', 'partyBTarget', 'partyBLocation', 'action'];
    this.dataSource = [];

    if (this.loadedTargets !== null && this.loadedTargets !== undefined && this.loadedAreas !== null && this.loadedAreas !== undefined) {
      this.showSpinner = false;
    }

    console.log('AllWarehouses');
    this.faunaAdminClientService.getAllWarehouses().subscribe(value => {
      console.log('data', value);
    });

    this.filteredAOptions = this.cdrCreatorForm.get('partyATarget').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
  }

  private _filter(value: string): string[] {
    if (value == null || value === undefined) { return this.loadedTargetANames.slice(); }
    const filterValue = value.toLowerCase();
    return this.loadedTargetANames.filter(option => option.toLowerCase().includes(filterValue));
  }

  assignPartyData(type: string, selectedIdentities: TargetDetailsModel) {
    console.log(type, selectedIdentities);

    if (selectedIdentities != null) {
      console.log(selectedIdentities.msisdn);
      console.log(selectedIdentities.imei);
      console.log(selectedIdentities.imsi);
    }
  }

  increaseWithinGivenMilisec(givenDate: string, delay: number): Date {
    let javaScriptRelease = Date.parse(givenDate);

    javaScriptRelease += this.getRandomInt(delay);
    const updateDate = new Date(0);
    updateDate.setTime(javaScriptRelease);

    return updateDate;
  }

  increaseDuration(duration: string): number {
    let tempVal = 10;
    if (duration !== null && duration !== undefined) {
      tempVal = parseInt(duration);
    }

    return tempVal + 1;
  }

  onFileSelect(input: HTMLInputElement) {
    const files = input.files;

    if (files && files.length) {
      const fileToRead = files[0];

      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        this.onFileLoad(e); //
      };
      fileReader.readAsText(fileToRead, 'UTF-8');
    }
  }

  onFileLoad(fileLoadedEvent) {
    const textFromFileLoaded = fileLoadedEvent.target.result;
    console.log('data', textFromFileLoaded);
  }

  createOneRecordEveryArea() {
    const areaSize = this.loadedAreas.length;
    let duration = this.cdrCreatorForm.get('callDuration').value;
    let givenDate = this.cdrCreatorForm.get('callDate').value;
    if (givenDate === null || givenDate === undefined) {
      alert('Given date undefined!');
    }

    const partyAName = this.cdrCreatorForm.get('partyATarget').value;
    if (partyAName === null || partyAName === undefined) {
      alert('partyA undefined!');
    }

    const partyA = this.getTargetModelByPartyName(partyAName);
    let targetIndexNum = 0;

    let cdr: CdrModel;

    for (let i = 0; i < areaSize; i++) {
      targetIndexNum = this.getRandomInt(this.loadedAreas.length);
      const areaPartyB = this.loadedAreas[targetIndexNum];
      givenDate = this.increaseWithinGivenMilisec(givenDate, 1800000); // 30 minutes in miliseconds
      duration = this.increaseDuration(duration);
      const areaPartyA = this.loadedAreas[i];

      targetIndexNum = this.getRandomInt(this.loadedTargets.length);
      let partyB = this.loadedTargets[targetIndexNum];

      while (partyA.name === partyB.name) {
        targetIndexNum = this.getRandomInt(this.loadedTargets.length);
        partyB = this.loadedTargets[targetIndexNum];
      }

      cdr = this.generatorService.buildCdrWithStr(givenDate, duration, partyA, partyB, areaPartyA, areaPartyB);
      this.records.push(cdr);

      const datePipeStringColumn = this.datePipe.transform(givenDate, this.displayDateFormat);
      const datePipeStringColumnKey = this.datePipe.transform(givenDate, 'yyyyMMddHHmmss');
      const newData: CdrElement = {
        id: datePipeStringColumnKey + '_' + duration + '_' + partyA.MSISDN[0],
        callDate: datePipeStringColumn,
        callDuration: duration,
        partyATarget: partyA.name,
        partyALocation: areaPartyA.name,
        partyBTarget: partyB.name,
        partyBLocation: areaPartyB.name
      };

      const tempDatasource = this.dataSource;
      tempDatasource.push(newData);

      this.dataSource = tempDatasource;
      this.dataSource = this.dataSource.slice();
    }
  }

  addRecords(): void {
    this.printContent();
    const cdrModel = this.generatorService.buildCdrWithGroup(this.cdrCreatorForm,
      this.partyAData,
      this.partyBData,
      this.partyALocation,
      this.partyBLocation);
    this.records.push(cdrModel);
    console.log('listed records: ', this.records);
    const datePipeStringColumn = this.datePipe.transform(this.cdrCreatorForm.get('callDate').value, this.displayDateFormat);
    const datePipeStringColumnKey = this.datePipe.transform(this.cdrCreatorForm.get('callDate').value, 'yyyyMMddHHmmss');
    const newData: CdrElement = {
      id: datePipeStringColumnKey + '_' + this.cdrCreatorForm.get('callDuration').value + '_' + this.partyAData.MSISDN[0],
      callDate: datePipeStringColumn,
      callDuration: this.cdrCreatorForm.get('callDuration').value,
      partyATarget: this.cdrCreatorForm.get('partyATarget').value,
      partyALocation: this.cdrCreatorForm.get('partyALocation').value,
      partyBTarget: this.cdrCreatorForm.get('partyBTarget').value,
      partyBLocation: this.cdrCreatorForm.get('partyBLocation').value
    };

    const tempDatasource = this.dataSource;
    tempDatasource.push(newData);

    this.dataSource = tempDatasource;
    this.dataSource = this.dataSource.slice();
    console.log('new Data', newData);
  }

  createRandomData() {
    let targetIndexNum = this.getRandomInt(this.loadedTargets.length);
    this.partyAData = this.loadedTargets[targetIndexNum];
    targetIndexNum = this.getRandomInt(this.loadedTargets.length);
    this.partyBData = this.loadedTargets[targetIndexNum];

    let areaIndexNum = this.getRandomInt(this.loadedAreas.length);
    this.partyALocation = this.loadedAreas[areaIndexNum];
    areaIndexNum = this.getRandomInt(this.loadedAreas.length);
    this.partyBLocation = this.loadedAreas[areaIndexNum];

    console.log('random target', this.loadedTargets[targetIndexNum]);

    this.cdrCreatorForm.patchValue({
      callDate:
        new Date(2019,
          this.getRandomInt(12),
          this.getRandomInt(28),
          this.getRandomInt(59),
          this.getRandomInt(59),
          this.getRandomInt(59),
          this.getRandomInt(999)),
      callDuration: this.getRandomInt(10000),
      partyATarget: this.partyAData.name,
      partyBTarget: this.partyBData.name,
      partyALocation: this.partyALocation.name,
      partyBLocation: this.partyBLocation.name
    });
    console.log('randomized data');
  }

  onReset(): void {
    this.cdrCreatorForm.reset();
    this.partyAData = null;
    this.partyBData = null;
    this.partyALocation = null;
    this.partyBLocation = null;
    this.jsonfileInputVariable.nativeElement.value = '';
  }

  onResetAll(): void {
    this.onReset();
    this.dataSource = [];
    this.records = [];
    this.jsonfileInputVariable.nativeElement.value = '';
  }

  getRandomInt(max): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

  increaseValue(incrementalValue) {
    let tempVal = this.cdrCreatorForm.get('callDuration').value;
    if (tempVal === null || tempVal === undefined) {
      tempVal = 0;
    }
    let value: number = parseInt(tempVal);
    value += incrementalValue;
    this.cdrCreatorForm.get('callDuration').patchValue(value);
  }

  decreaseValue(decrementalValue) {
    let tempVal = this.cdrCreatorForm.get('callDuration').value;
    if (tempVal === null || tempVal === undefined) {
      tempVal = 0;
    }
    let value: number = parseInt(tempVal);
    value -= decrementalValue;
    this.cdrCreatorForm.get('callDuration').patchValue(value);
  }

  getTargetModelByPartyName(partyName: string) {
    let targetModel: TargetModel;
    for (const entry of this.loadedTargets) {
      if (entry.name === partyName) {
        targetModel = {
          MSISDN: entry.MSISDN.slice(),
          IMEI: entry.IMEI.slice(),
          IMSI: entry.IMSI.slice(),
          name: entry.name,
          mcngId: entry.mcngId
        };
        return targetModel;
      }
    }
    alert('Failed to find target with name: ' + partyName);
  }


  printContent(): void {
    console.log('callDate:' + this.cdrCreatorForm.get('callDate').value);
    console.log('callDuration:' + this.cdrCreatorForm.get('callDuration').value);
    console.log('partyATarget:' + this.cdrCreatorForm.get('partyATarget').value);
    console.log('partyALocation:' + this.cdrCreatorForm.get('partyALocation').value);
    console.log('partyBTarget:' + this.cdrCreatorForm.get('partyBTarget').value);
    console.log('partyBLocation:' + this.cdrCreatorForm.get('partyBLocation').value);
  }

  preparePartyAData(): void {
    const selection: string = this.cdrCreatorForm.get('partyATarget').value;
    console.log('A selection data', selection);
    for (const entry of this.loadedTargets) {
      if (entry.name === selection) {
        this.partyAData = {
          MSISDN: entry.MSISDN.slice(),
          IMEI: entry.IMEI.slice(),
          IMSI: entry.IMSI.slice(),
          name: entry.name,
          mcngId: entry.mcngId
        };
        return;
      }
      this.partyAData = null;
    }
  }

  preparePartyALocation(): void {
    console.log('Onselect');
    const selection: string = this.cdrCreatorForm.get('partyALocation').value;
    for (const entry of this.loadedAreas) {
      if (entry.name === selection) {
        this.partyALocation = {
          cgi: entry.cgi,
          long: entry.long,
          lat: entry.lat,
          name: entry.name
        };
      }
    }
  }

  preparePartyBLocation(): void {
    const selection: string = this.cdrCreatorForm.get('partyBLocation').value;

    for (const entry of this.loadedAreas) {
      if (entry.name === selection) {
        this.partyBLocation = {
          cgi: entry.cgi,
          long: entry.long,
          lat: entry.lat,
          name: entry.name
        };
      }
    }
  }

  preparePartyBData(): void {
    const selection: string = this.cdrCreatorForm.get('partyBTarget').value;
    console.log('A selection data', selection);
    for (const entry of this.loadedTargets) {
      if (entry.name === selection) {
        this.partyBData = {
          MSISDN: entry.MSISDN.slice(),
          IMEI: entry.IMEI.slice(),
          IMSI: entry.IMSI.slice(),
          name: entry.name,
          mcngId: entry.mcngId
        };
        return;
      }
      this.partyBData = null;
    }
  }

  moveUp(rowObject) {
    console.log('[moveUp] rowObject', rowObject);
  }

  moveDown(dataIndex, rowObject) {
    // console.log(this.arrayMove([1, 2, 3], 1, 2));
    console.log('[moveDown] rowObject', rowObject);
    console.log('[moveDown] index', dataIndex);
  }

  // arrayMove(arr, old_index, new_index) {
  //   if (new_index >= arr.length) {
  //     let k = new_index - arr.length + 1;
  //     while (k--) {
  //       arr.push(undefined);
  //     }
  //   }
  //   arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
  //
  //   return arr; // for testing
  // }

  edit(rowObject) {
    this.cdrCreatorForm.get('partyBLocation').patchValue(rowObject.partyBLocation);
    this.cdrCreatorForm.get('partyBTarget').patchValue(rowObject.partyBTarget);
    this.cdrCreatorForm.get('partyALocation').patchValue(rowObject.partyALocation);
    this.cdrCreatorForm.get('partyATarget').patchValue(rowObject.partyATarget);
    this.cdrCreatorForm.get('callDate').patchValue(rowObject.callDate);
    this.cdrCreatorForm.get('callDuration').patchValue(rowObject.callDuration);
    this.preparePartyAData();
    this.preparePartyBData();
    this.preparePartyALocation();
    this.preparePartyBLocation();
    console.log('[edit] rowObject', rowObject);
  }

  removeRowData(rowObject) {
    this.records = this.records.filter(value => {
      const id = value.START + '_' + value.DURATION + '_' + value.MSISDNA;
      return (id !== rowObject.id);
    });

    this.dataSource = this.dataSource.filter((value, key) => {
      return value.id !== rowObject.id;
    });

    // console.log('this.records size: ' + this.records.length);
    // console.log('this.dataSource size: ' + this.dataSource.length);
  }

  generateCDR() {
    console.log('generate cdr');
    const data: string = this.generatorService.buildCdrJson(this.records);
    this.generatorService.generateCdrCsv(data).subscribe(value => {
      // console.log('generated cdr csv!', value);
      const blob = new Blob([value], {type: 'application/octet-stream'});
      this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
      console.log('[generateCDR]' + this.fileUrl);
    });
  }

  generateMcngEtlRecord() {
    console.log('Test!!!!');
    this.generateMcngEtlService.generateMcngEtl(null);
  }

  generateCdrMcngConfigDialog() {
    const dialogRef = this.dialog.open(GenerateMcngConfigDialogComponent, {
      width: '250px'
    });
  }

  openAddAreaDialog() {
    const dialogRef = this.dialog.open(AddAreaComponent, {
      width: '250px'
    });
  }

  downloadFile(): void {
    this.generateMcngEtlService.downloadRawData(this.records);
    this.generateMcngEtlService.generateMfiCdr(this.records);
    this.generateMcngEtlService.downloadMcngEtlFiles(this.records);
  }

  ngOnDestroy(): void {
    this.areaModelSubscription.unsubscribe();
    this.targetModelSubscription.unsubscribe();
    this.targetModelSubjectSubscription.unsubscribe();
  }
}
