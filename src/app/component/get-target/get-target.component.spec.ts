import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetTargetComponent } from './get-target.component';

describe('GetTargetComponent', () => {
  let component: GetTargetComponent;
  let fixture: ComponentFixture<GetTargetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetTargetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetTargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
