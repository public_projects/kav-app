import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdrCreatorTableComponent } from './cdr-creator-table.component';

describe('CdrCreatorTableComponent', () => {
  let component: CdrCreatorTableComponent;
  let fixture: ComponentFixture<CdrCreatorTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdrCreatorTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdrCreatorTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
