import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateMcngConfigDialogComponent } from './generate-mcng-config-dialog.component';

describe('GenerateMcngConfigDialogComponent', () => {
  let component: GenerateMcngConfigDialogComponent;
  let fixture: ComponentFixture<GenerateMcngConfigDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateMcngConfigDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateMcngConfigDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
