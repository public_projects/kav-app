import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UsersData} from '../dialog-box/dialog-box.component';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-generate-mcng-config-dialog',
  templateUrl: './generate-mcng-config-dialog.component.html',
  styleUrls: ['./generate-mcng-config-dialog.component.css']
})
export class GenerateMcngConfigDialogComponent {
  action: string;
  inputData: any;
  generatorGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<GenerateMcngConfigDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: UsersData) {
    console.log(data);
    this.inputData = {...data};
    this.action = this.inputData.action;
  }

  doAction() {
    this.dialogRef.close({event: this.action, data: this.inputData});
  }

  generateMcng() {
    console.log('Generate mcng');
  }

  onReset() {
    console.log('Reset mcng');
  }
}
