import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadMfiCdrComponent } from './upload-mfi-cdr.component';

describe('UploadMfiCdrComponent', () => {
  let component: UploadMfiCdrComponent;
  let fixture: ComponentFixture<UploadMfiCdrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadMfiCdrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadMfiCdrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
