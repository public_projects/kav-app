import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CdrModel} from '../../model/CdrModel';
import {AreaService} from '../../service/area.service';
import {AreaModel} from '../../model/AreaModel';
import {CdrElement} from '../../model/CdrElement';
import {TargetService} from '../../service/target.service';
import {TargetModel} from '../../model/TargetModel';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ShowPartyInfoComponent} from '../show-party-info/show-party-info.component';

@Component({
  selector: 'app-upload-mfi-cdr',
  templateUrl: './upload-mfi-cdr.component.html',
  styleUrls: ['./upload-mfi-cdr.component.css']
})
export class UploadMfiCdrComponent implements OnInit, OnDestroy {
  loadedAreas: AreaModel[];
  loadedTargets: TargetModel[];
  csvContent: string;
  dataSource: MatTableDataSource<CdrElement>;
  displayedColumns: string[];
  @ViewChild(MatSort) matSort: MatSort;
  @ViewChild('jsonfileInput')
  jsonfileInputVariable: ElementRef;
  areaModelSubjectSubscription: Subscription;
  areaModelSubscription: Subscription;
  targetModelSubjectSubscription: Subscription;
  dataSourceIsValid = false;

  constructor(private areaService: AreaService, private targetService: TargetService, private dialog: MatDialog) {
  }

  ngOnInit() {
    console.log('2. Load target!');
    this.targetService.getTargets().subscribe(targetModels => {
      this.loadedTargets = targetModels;
      console.log('Loaded targets;', this.loadedTargets);
    });

    this.areaModelSubjectSubscription = this.areaService.areaModelSubject.subscribe(areaModels => {
      this.loadedAreas = areaModels;
      console.log('[fethed][subject][area][value]', this.loadedAreas);
    });

    console.log('2. Load area!');
    this.areaModelSubscription = this.areaService.getAreas().subscribe(areaModels => {
        this.loadedAreas = areaModels;
        console.log('[fethed][area][value]', this.loadedAreas);
      }
    );

    this.displayedColumns = ['num', 'callDate', 'callDuration', 'partyATarget', 'partyALocation', 'partyBTarget', 'partyBLocation'];

    this.targetModelSubjectSubscription = this.targetService.targetSubject.subscribe(targetModels => this.loadedTargets = targetModels);
  }

  getPartyInfo(partyName: string): string {
    for (const loadedTarget of this.loadedTargets) {
      if (loadedTarget.name === partyName) {
        return JSON.stringify(loadedTarget, null, '\t');
      }
    }
    return;
  }

  openPartyInfoDialog(partyName: string) {
    console.log('partyName', partyName);
    let selectedElement: TargetModel = null;

    for (const loadedTarget of this.loadedTargets) {
      if (loadedTarget.name === partyName) {
        selectedElement = loadedTarget;
      }
    }

    console.log('selectedParty: ', selectedElement);

    if (selectedElement !== null) {
      const dialogRef = this.dialog.open(ShowPartyInfoComponent, {
        width: '250px',
        data: {selectedElement}
      });
    }
  }

  openAreaInfoDialog(areaName: string) {
    console.log('areaName', areaName);
    let selectedElement: AreaModel = null;

    for (const loadedArea of this.loadedAreas) {
      if (loadedArea.name === areaName) {
        selectedElement = loadedArea;
      }
    }

    console.log('selectedArea: ', selectedElement);

    if (selectedElement !== null) {
      const dialogRef = this.dialog.open(ShowPartyInfoComponent, {
        width: '250px',
        data: {selectedElement}
      });
    }
  }

  getAreaInfo(locationName: string): string {
    for (const loadedArea of this.loadedAreas) {
      if (loadedArea.name === locationName) {
        return JSON.stringify(loadedArea, null, '\t');
      }
    }
    return;
  }

  onFileLoad(fileLoadedEvent) {
    const textFromFileLoaded = fileLoadedEvent.target.result;
    this.csvContent = textFromFileLoaded;
    const jsonObject = JSON.parse(textFromFileLoaded);

    let tempDatasource: CdrElement[] = [];
    if (this.dataSource !== null && this.dataSource !== undefined && this.dataSource.data !== undefined) {
      tempDatasource = this.dataSource.data;
    }

    if (jsonObject !== null && jsonObject.length > 0) {
      for (const obj of jsonObject) {
        // console.log(obj);

        const cdrModel = new CdrModel();
        cdrModel.START = obj.START;
        cdrModel.DURATION = obj.DURATION;
        cdrModel.MSISDNA = obj.MSISDNA;
        cdrModel.IMEIA = obj.IMEIA;
        cdrModel.IMSIA = obj.IMSIA;
        cdrModel.MSISDNB = obj.MSISDNB;
        cdrModel.IMEIB = obj.IMEIB;
        cdrModel.IMSIB = obj.IMSIB;
        cdrModel.CGISTARTA = obj.CGISTARTA;
        cdrModel.LATSTARTA = obj.LATSTARTA;
        cdrModel.LONSTARTA = obj.LONSTARTA;
        cdrModel.CGISTARTB = obj.CGISTARTB;
        cdrModel.LATSTARTB = obj.LATSTARTB;
        cdrModel.LONSTARTB = obj.LONSTARTB;

        const {areaAName, areaBName} = this.extractedAreaDetails(cdrModel);
        const {targetAName, targetBName} = this.extractedTargetDetails(cdrModel);

        const newData: CdrElement = {
          callDate: this.formatDate(cdrModel.START),
          callDuration: Number(cdrModel.DURATION),
          partyATarget: targetAName,
          partyALocation: areaAName,
          partyBTarget: targetBName,
          partyBLocation: areaBName
        };
        tempDatasource.push(newData);
      }

      this.dataSource = new MatTableDataSource<CdrElement>(tempDatasource);
      this.dataSource.sort = this.matSort;
      this.dataSourceIsValid = true;
    }
  }

  private extractedTargetDetails(cdrModel: CdrModel) {
    let targetAName = null;
    let targetBName = null;
    for (const loadedTarget of this.loadedTargets) {
      if (targetAName !== null && targetBName !== null) {
        break;
      }

      if (loadedTarget.MSISDN[0] === cdrModel.MSISDNA) {
        targetAName = loadedTarget.name;
      }

      if (loadedTarget.MSISDN[0] === cdrModel.MSISDNB) {
        targetBName = loadedTarget.name;
      }
    }

    if (targetAName === null) {
      targetAName = 'MSISDN:' + cdrModel.MSISDNA + ', \n IMEI:' + cdrModel.IMEIA + ', \n IMSI:' + cdrModel.IMSIA;
    }

    if (targetBName === null) {
      targetBName = 'MSISDN:' + cdrModel.MSISDNB + ', \n IMEI:' + cdrModel.IMEIB + ', \n IMSI:' + cdrModel.IMSIB;
    }

    return {targetAName, targetBName};
  }

  private extractedAreaDetails(cdrModel: CdrModel) {
    let areaAName = null;
    let areaBName = null;
    for (const loadedArea of this.loadedAreas) {
      if (areaAName !== null && areaBName !== null) {
        break;
      }

      if (loadedArea.cgi === cdrModel.CGISTARTA) {
        areaAName = loadedArea.name;
      }

      if (loadedArea.cgi === cdrModel.CGISTARTB) {
        areaBName = loadedArea.name;
      }
    }

    if (areaAName === null) {
      areaAName = 'cgi:' + cdrModel.CGISTARTA + ', \n lat:' + cdrModel.LATSTARTA + ', \n long:' + cdrModel.LONSTARTA;
    }

    if (areaBName === null) {
      areaBName = 'cgi:' + cdrModel.CGISTARTB + ', \n lat:' + cdrModel.LATSTARTB + ', \n long:' + cdrModel.LONSTARTB;
    }
    return {areaAName, areaBName};
  }

  formatDate(originalDate: string): string {
    const year = originalDate.substr(0, 4);
    const month = originalDate.substr(4, 2);
    const day = originalDate.substr(6, 2);
    const hour = originalDate.substr(8, 2);
    const minute = originalDate.substr(10, 2);
    const second = originalDate.substr(12);
    return day + '.' + month + '.' + year + ' ' + hour + ':' + minute + ':' + second;
  }

  onFileSelect(input: HTMLInputElement) {

    const files = input.files;
    const content = this.csvContent;

    if (files && files.length) {
      /*
       console.log("Filename: " + files[0].name);
       console.log("Type: " + files[0].type);
       console.log("Size: " + files[0].size + " bytes");
       */

      const fileToRead = files[0];

      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        this.onFileLoad(e);
      };
      fileReader.readAsText(fileToRead, 'UTF-8');
    }
  }

  ngOnDestroy() {
    this.targetModelSubjectSubscription.unsubscribe();
    this.areaModelSubjectSubscription.unsubscribe();
    this.areaModelSubscription.unsubscribe();
  }

  onResetAll(): void {
    this.dataSource = new MatTableDataSource<CdrElement>();
    this.dataSourceIsValid = false;
    this.jsonfileInputVariable.nativeElement.value = '';
  }
}


