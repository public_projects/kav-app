import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AreaService} from '../../service/area.service';
import {AreaModel} from '../../model/AreaModel';

@Component({
  selector: 'app-add-area',
  templateUrl: './add-area.component.html',
  styleUrls: ['./add-area.component.css']
})
export class AddAreaComponent implements OnInit {
  areaForm: FormGroup;

  constructor(private areaService: AreaService) {
  }

  ngOnInit(): void {
    this.areaForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      cgi: new FormControl(null, Validators.required),
      latitude: new FormControl(null, Validators.required),
      longitude: new FormControl(null, Validators.required),
    });
  }

  addRecord(): void {
    console.log(this.areaForm);
    const areaModel: AreaModel = {
      name: this.areaForm.get('name').value,
      cgi: this.areaForm.get('cgi').value,
      lat: this.areaForm.get('latitude').value,
      long: this.areaForm.get('longitude').value
    };
    // this.areaService.saveArea(areaModel);
    this.areaService.saveArea2(areaModel).subscribe(value => {
      console.log('value: ' + value);
    }, error => {
      console.log(error);
      alert('Save error: ' + error);
    });
  }

  onReset(): void {
    this.areaForm.reset();
  }
}
