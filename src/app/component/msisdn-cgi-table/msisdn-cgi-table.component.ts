import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import { MsisdnCgiTableDataSource } from './msisdn-cgi-table-datasource';
import {CgiActivityService} from '../../service/cgi-activity.service';
import {DataModel1} from '../../model/DataModel1';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-msisdn-cgi-table',
  templateUrl: './msisdn-cgi-table.component.html',
  styleUrls: ['./msisdn-cgi-table.component.css']
})
export class MsisdnCgiTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatTable) table: MatTable<DataModel1>;
  @ViewChild(MatSort) matSort: MatSort;
  dataSource: MsisdnCgiTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'key', 'key2', 'column1', 'column2'];
  constructor(private cgiActivityService: CgiActivityService) {
    console.log('loading 1');
  }

  ngOnInit() {
    this.dataSource = new MsisdnCgiTableDataSource(this.cgiActivityService, this.matSort);
    console.log('loading 2');
  }

  ngAfterViewInit() {
    this.table.dataSource = this.dataSource;
  }
}
