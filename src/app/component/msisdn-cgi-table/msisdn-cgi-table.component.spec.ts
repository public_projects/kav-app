import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';

import { MsisdnCgiTableComponent } from './msisdn-cgi-table.component';

describe('MsisdnCgiTableComponent', () => {
  let component: MsisdnCgiTableComponent;
  let fixture: ComponentFixture<MsisdnCgiTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MsisdnCgiTableComponent ],
      imports: [
        NoopAnimationsModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MsisdnCgiTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
