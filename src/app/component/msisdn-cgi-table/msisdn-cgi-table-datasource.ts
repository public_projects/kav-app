import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {Observable, of as observableOf, merge, BehaviorSubject} from 'rxjs';
import {DataModel1} from '../../model/DataModel1';
import {CgiActivityService} from '../../service/cgi-activity.service';

/**
 * Data source for the MsisdnCgiTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class MsisdnCgiTableDataSource extends DataSource<DataModel1> {
  paginator: MatPaginator;
  private msisdnCgisSubject = new BehaviorSubject<DataModel1[]>([]);

  constructor(private cgiActivityService: CgiActivityService, private sort: MatSort) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<DataModel1[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    this.cgiActivityService.getCgiActivitiesObserver()
      .subscribe(response => {
      this.msisdnCgisSubject.next(response);
    });

    return this.msisdnCgisSubject;
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {
    this.msisdnCgisSubject.complete();
    this.msisdnCgisSubject.observers = [];
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
