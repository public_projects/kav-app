import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchDataGeneratorComponent } from './batch-data-generator.component';

describe('BatchDataGeneratorComponent', () => {
  let component: BatchDataGeneratorComponent;
  let fixture: ComponentFixture<BatchDataGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchDataGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchDataGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
