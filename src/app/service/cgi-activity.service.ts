import {Injectable} from '@angular/core';
import {DataModel1} from '../model/DataModel1';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CgiActivityService {

  constructor(private http: HttpClient) {
  }

  getCgiActivities(): DataModel1[] {
    const data = new Array<DataModel1>();
    this.http
      .get<{ [key: string]: DataModel1 }>('http://localhost:9011/msisdn/activities')
      .pipe(map(responseData => {
          const dataModel1Array: DataModel1[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              dataModel1Array.push({...responseData[key], id: key});
            }
          }
          return dataModel1Array;
        }
      ))
      .subscribe(responseData => {
        return responseData;
      });
    return data; // return empty list
  }

    /**
     * Getting data from https://gitlab.com/public_projects/kav-cassandra-helper.git
     */
  getCgiActivitiesObserver(): Observable<DataModel1[]> {
    return this.http
      .get<{ [key: string]: DataModel1 }>('http://localhost:9011/msisdn/activities')
      .pipe(map(responseData => {
          const dataModel1Array: DataModel1[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              dataModel1Array.push({...responseData[key], id: key});
            }
          }
          return dataModel1Array;
        }
      ));
  }
}
