import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CdrModel} from '../model/CdrModel';
import {FormGroup} from '@angular/forms';
import {TargetModel} from '../model/TargetModel';
import {AreaModel} from '../model/AreaModel';
import {DatePipe} from '@angular/common';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeneratorService {

  static setDefaultValue(
    cdr: CdrModel,
    duration: string,
    partyATarget: TargetModel,
    partyBTarget: TargetModel,
    partyAAreaModel: AreaModel,
    partyBAreaModel: AreaModel): CdrModel {

    return cdr;
  }

  constructor(private http: HttpClient, private datePipe: DatePipe) {
  }

  buildCdrWithGroup(cdrFormGroup: FormGroup,
                    partyATarget: TargetModel,
                    partyBTarget: TargetModel,
                    partyAAreaModel: AreaModel,
                    partyBAreaModel: AreaModel): CdrModel {
    const cdr = new CdrModel();
    console.log('the selected targetA mcngId', partyATarget.mcngId);
    cdr.PARTY_A_MCNG_ID = partyATarget.mcngId;
    cdr.PARTY_A_NAME = partyATarget.name;
    cdr.PARTY_B_MCNG_ID = partyBTarget.mcngId;
    cdr.PARTY_B_NAME = partyBTarget.name;
    cdr.PROVIDER = 'Movilnet';
    cdr.EVENTTYPE = 'VOICE';
    cdr.START = this.datePipe.transform(cdrFormGroup.get('callDate').value, 'yyyyMMddHHmmss');
    cdr.DURATION = cdrFormGroup.get('callDuration').value;
    cdr.STATUS = 'CALLING';
    cdr.CGISTARTA = partyAAreaModel.cgi;
    cdr.CGISTARTB = partyBAreaModel.cgi;
    cdr.LATSTARTA = partyAAreaModel.lat;
    cdr.LATSTARTB = partyBAreaModel.lat;
    cdr.LONSTARTA = partyAAreaModel.long;
    cdr.LONSTARTB = partyBAreaModel.long;
    cdr.CGIENDA = partyAAreaModel.cgi;
    cdr.CGIENDB = partyBAreaModel.cgi;
    cdr.LATENDA = partyAAreaModel.lat;
    cdr.LATENDB = partyBAreaModel.lat;
    cdr.LONENDA = partyAAreaModel.long;
    cdr.LONENDB = partyBAreaModel.long;
    cdr.MSISDNA = partyATarget.MSISDN[0];
    cdr.MSISDNB = partyBTarget.MSISDN[0];
    cdr.MSISDNC = '';
    cdr.IMEIA = partyATarget.IMEI[0];
    cdr.IMEIB = partyBTarget.IMEI[0];
    cdr.IMSIA = partyATarget.IMSI[0];
    cdr.IMSIB = partyBTarget.IMSI[0];
    return cdr;
  }

  buildCdrWithStr(dateStr: string,
                  durationStr: string,
                  partyATarget: TargetModel,
                  partyBTarget: TargetModel,
                  partyAAreaModel: AreaModel,
                  partyBAreaModel: AreaModel): CdrModel {
    let cdr = new CdrModel();
    cdr.PROVIDER = 'Movilnet';
    cdr.EVENTTYPE = 'VOICE';
    cdr.START = this.datePipe.transform(dateStr, 'yyyyMMddHHmmss');
    cdr.DURATION = durationStr;
    cdr.STATUS = 'CALLING';
    cdr.CGISTARTA = partyAAreaModel.cgi;
    cdr.CGISTARTB = partyBAreaModel.cgi;
    cdr.LATSTARTA = partyAAreaModel.lat;
    cdr.LATSTARTB = partyBAreaModel.lat;
    cdr.LONSTARTA = partyAAreaModel.long;
    cdr.LONSTARTB = partyBAreaModel.long;
    cdr.CGIENDA = partyAAreaModel.cgi;
    cdr.CGIENDB = partyBAreaModel.cgi;
    cdr.LATENDA = partyAAreaModel.lat;
    cdr.LATENDB = partyBAreaModel.lat;
    cdr.LONENDA = partyAAreaModel.long;
    cdr.LONENDB = partyBAreaModel.long;
    cdr.MSISDNA = partyATarget.MSISDN[0];
    cdr.MSISDNB = partyBTarget.MSISDN[0];
    cdr.MSISDNC = '';
    cdr.IMEIA = partyATarget.IMEI[0];
    cdr.IMEIB = partyBTarget.IMEI[0];
    cdr.IMSIA = partyATarget.IMSI[0];
    cdr.IMSIB = partyBTarget.IMSI[0];

    return cdr;
  }

  generateCdrCsv(requestBody: string): Observable<any> {
    return this.http.post<CdrResponse>('http://localhost:8982/json/to/csv', requestBody)
      .pipe(map(source => {
        const csvData = source.response;
        console.log('POST call successful value returned in body', csvData);
        return csvData;
      }));
  }

  buildCdrJson(requestBody: CdrModel[]): string {
    return JSON.stringify(requestBody, null, 2);
  }
}

interface CdrResponse {
  response: string;
}
