import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {AreaModel} from "../model/AreaModel";
import {map} from "rxjs/operators";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {TargetModel} from "../model/TargetModel";

@Injectable({
  providedIn: 'root'
})
export class FaunaAdminClientService {

  constructor(private http: HttpClient) { }

  getAllWarehouses(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer fnAD12AgjzACAd4BL-6ucVm6Dpyf-wkjDBERJ3tp'
      })
    };

    const requestBody =  {query: 'query {\n' +
        '  allWarehouses {\n' +
        '    data {\n' +
        '      _id\n' +
        '      name\n' +
        '      address {\n' +
        '        street\n' +
        '        city\n' +
        '        state\n' +
        '        zipCode\n' +
        '      }\n' +
        '    }\n' +
        '  }\n' +
        '}',
      variables: {}
    };

    return this.http
      .post('https://graphql.fauna.com/graphql', requestBody, httpOptions)
      .pipe(
        map(responseData => {
          console.log('responseData', responseData);
          return responseData;
        }));
  }

}
