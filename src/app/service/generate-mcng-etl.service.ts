import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { CallActivityModel } from '../model/CallActivityModel';
import { CdrModel } from "../model/CdrModel";
import { MsisdnFlatFileBuilder } from './mcngetl/msisdn-flat-file-builder';
import { TargetFlatFileBuilder } from './mcngetl/target-flat-file-builder';

@Injectable({
  providedIn: 'root'
})
export class GenerateMcngEtlService {

  constructor(private http: HttpClient) {
  }

  generateMcngEtl(callActivityModels: CallActivityModel[]): void {
    const requestAreaModels: CallActivityModel[] = [];
    this.http.post('http://localhost:8982/generate/mcng/etl/activity',
      requestAreaModels)
      .subscribe(
        (val) => {
          console.log('POST call successful value returned in body',
            val);

          return new Observable((observer) => {
            observer.next('Success!');
          });
        },
        response => {
          console.log('POST call in error', response);
          return new Observable((observer) => {
            observer.error(response);
          });
        },
        () => {
          console.log('The POST observable is now completed.');
        });
  }

  downloadMcngEtlFiles(records: CdrModel[]): void {
    let targets: string[] = new Array();
    const msisdnBuilder = new MsisdnFlatFileBuilder();
    const targetBuilder = new TargetFlatFileBuilder();

    for (const record of records) {
      let today = new Date();
      if (!targets.includes(record.PARTY_A_MCNG_ID)) {
        msisdnBuilder.append(record.PARTY_A_MCNG_ID, record.MSISDNA, this.getFlatFileTimeFormat(today));
        targetBuilder.append(record.PARTY_A_MCNG_ID, record.PARTY_A_NAME, this.getFlatFileTimeFormat(today));
        targets.push(record.PARTY_A_MCNG_ID);
      }

      if (!targets.includes(record.PARTY_B_MCNG_ID)) {
        msisdnBuilder.append(record.PARTY_B_MCNG_ID, record.MSISDNB, this.getFlatFileTimeFormat(today));
        targetBuilder.append(record.PARTY_B_MCNG_ID, record.PARTY_B_NAME, this.getFlatFileTimeFormat(today));
        targets.push(record.PARTY_B_MCNG_ID);
      }
    }

    this.download(msisdnBuilder.getFileName(), msisdnBuilder.build());
    this.download(targetBuilder.getFileName(), targetBuilder.build());
  }

  generateMfiCdr(records: CdrModel[]) {
    const HEADER = "PROVIDER;EVENTTYPE;START;DURATION;STATUS;CGISTARTA;CGISTARTB;LATSTARTA;LATSTARTB;LONSTARTA;LONSTARTB;CGIENDA;CGIENDB;LATENDA;LATENDB;LONENDA;LONENDB;MSISDNA;MSISDNB;MSISDNC;IMEIA;IMEIB;IMSIA;IMSIB";
    let csv = HEADER + '\n';
    for (const record of records) {
      csv += record.PROVIDER + ';'
        + record.EVENTTYPE + ';'
        + record.START + ';'
        + record.DURATION + ';'
        + record.STATUS + ';'
        + record.CGISTARTA + ';'
        + record.CGISTARTB + ';'
        + record.LATSTARTA + ';'
        + record.LATSTARTB + ';'
        + record.LONSTARTA + ';'
        + record.LONSTARTB + ';'
        + record.CGIENDA + ';'
        + record.CGIENDB + ';'
        + record.LATENDA + ';'
        + record.LATENDB + ';'
        + record.LONENDA + ';'
        + record.LONENDB + ';'
        + record.MSISDNA + ';'
        + record.MSISDNB + ';'
        + record.MSISDNC + ';'
        + record.IMEIA + ';'
        + record.IMEIB + ';'
        + record.IMSIA + ';'
        + record.IMSIB + '\n';
    }

    this.download(this.getFlatFileTimeFormat(new Date()) + '_mfi_cdr.csv', csv);
  }



  // downloadTargetFlatFile(records: CdrModel[]): void {
  //   const HEADER = '"MCTARGETID"|"NAME"|"DATEID"|"SYSTEMID"|"AGENCYID"';
  //   let csv = HEADER + '\n';
  //   let targets: string[] = new Array();

  //   let count = 1;
  //   for (const record of records) {
  //     let today = new Date();
  //     today.setSeconds(today.getSeconds() + ((count + 1) * 2));

  //     if (!targets.includes(record.PARTY_A_MCNG_ID)) {
  //       csv += record.PARTY_A_MCNG_ID + '|' + record.PARTY_A_NAME + '|' + this.getFlatFileTimeFormat(today) + '|' + 'mcng' + '|' + '3' + '\n';
  //       targets.push(record.PARTY_A_MCNG_ID);
  //     }

  //     if (!targets.includes(record.PARTY_B_MCNG_ID)) {
  //       today.setSeconds(today.getSeconds() + ((count + 1) * 2));
  //       csv += record.PARTY_B_MCNG_ID + '|' + record.PARTY_B_NAME + '|' + this.getFlatFileTimeFormat(today) + '|' + 'mcng' + '|' + '3' + '\n';
  //       targets.push(record.PARTY_B_MCNG_ID);
  //     }
  //   }

  //   const a = document.createElement('a');
  //   const fileName = 'P_TARGET_FLAT.csv';
  //   a.download = fileName;
  //   const blob: Blob = new Blob([csv], { type: 'text/csv' });
  //   a.href = window.URL.createObjectURL(blob);
  //   a.click();
  // }

  downloadRawData(records: CdrModel[]): void {
    this.download(this.getFlatFileTimeFormat(new Date()) + '_raw.csv', JSON.stringify(records, null, 2));
  }

  getFlatFileTimeFormat(date: Date): string {
    let newDateFormat = date.getFullYear() + '' +
      this.doubleDigitTimeFormat(date.getMonth() + '') + '' +
      this.doubleDigitTimeFormat(date.getDate() + '') + '' +
      this.doubleDigitTimeFormat(date.getHours() + '') + '' +
      this.doubleDigitTimeFormat(date.getMinutes() + '') + '' +
      this.doubleDigitTimeFormat(date.getSeconds() + '') + '' +
      date.getMilliseconds();

    return newDateFormat;
  }

  private download(fileName: string, content: string) {
    const a = document.createElement('a');
    a.download = fileName;
    const blob: Blob = new Blob([content], { type: 'text/csv' });
    a.href = window.URL.createObjectURL(blob);
    a.click();
  }

  doubleDigitTimeFormat(timeFormat: string): string {
    if (timeFormat.length === 1) {
      timeFormat = '0' + timeFormat;
    }
    return timeFormat;
  }
}