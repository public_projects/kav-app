import { TestBed } from '@angular/core/testing';

import { GenerateMcngEtlService } from './generate-mcng-etl.service';

describe('GenerateMcngEtlService', () => {
  let service: GenerateMcngEtlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenerateMcngEtlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
