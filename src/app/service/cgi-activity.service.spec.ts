import { TestBed } from '@angular/core/testing';

import { CgiActivityService } from './cgi-activity.service';

describe('CgiActivityService', () => {
  let service: CgiActivityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CgiActivityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
