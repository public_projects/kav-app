import { TestBed } from '@angular/core/testing';

import { FaunaAdminClientService } from './fauna-admin-client.service';

describe('FaunaAdminClientService', () => {
  let service: FaunaAdminClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FaunaAdminClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
