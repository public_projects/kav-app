import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {AreaModel} from "../model/AreaModel";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CgiService {

  constructor(private http: HttpClient) {
  }

  getArea(cgi: string): Observable<AreaModel> {
    return this.http
      .get<AreaModel>('https://greenhorn-c7a65.firebaseio.com/dev_db_1/cgi/' + cgi + '.json')
      .pipe(
        map(responseData => {
          return responseData;
        }));
  }
}
