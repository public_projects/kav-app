import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {TargetModel} from '../model/TargetModel';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TargetService {
  targetModels: TargetModel[];
  initialized = false;
  targetSubject: Subject<TargetModel[]> = new Subject<TargetModel[]>();

  // subscribe/ https://www.youtube.com/watch?v=vl5C3eMeHbY
  constructor(private http: HttpClient) {
  }

  getTargets(): Observable<TargetModel[]> {
    // Avoids fetching data always by storing it in 'targetModels'
    if (this.initialized) {
      return new Observable<TargetModel[]>(subscriber => {
        console.log('Returning existing targetModels');
        subscriber.next(this.targetModels);
      });
    }

    this.initialized = true;
    return this.http
      .get<{ [key: string]: TargetModel }>('https://green-db512-default-rtdb.asia-southeast1.firebasedatabase.app/dev_db_2/targets.json')
      .pipe(
        map(responseData => {
          const targetsArray: TargetModel[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              targetsArray.push({...responseData[key], name: key, mcngId: responseData[key]['mcng-etl-id']});
            }
          }
          console.log('Made new call to target');
          this.targetModels = targetsArray;
          this.targetSubject.next(this.targetModels);
          return this.targetModels;
        }));
  }

  // getTargets() {
  //     this.http
  //         .get('https://greenhorn-c7a65.firebaseio.com/dev_db_1/targets.json')
  //         .subscribe(targets => {
  //             console.log(targets);
  //         });
  // }
}
