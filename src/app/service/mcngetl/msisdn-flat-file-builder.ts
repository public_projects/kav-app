export class MsisdnFlatFileBuilder {
    HEADER: string;
    csv: string = '';

    constructor() {
        this.HEADER = '"MCTARGETID"|"MSISDNID"|"CREATED"|"ISASSIGNEDBYANALYSTS"|"SYSTEMID"|"MCAGENCYID"';
        this.csv += this.HEADER;
    }

    append(mcngId: string, msisdn: string, dateString: string): void {
        this.csv += '\n' + mcngId + '|' + msisdn + '|' + dateString + '|' + '1' + '|' + 'mcng' + '|' + '3';
    }

    build(): string {
        return this.csv;
    }

    getFileName(): string {
        return 'P_TARGET_MSISDN_FLAT.csv';
    }
}
