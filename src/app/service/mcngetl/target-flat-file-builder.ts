export class TargetFlatFileBuilder {
    HEADER: string;
    csv: string = '';

    constructor() {
        this.HEADER = '"MCTARGETID"|"NAME"|"DATEID"|"SYSTEMID"|"AGENCYID"';
        this.csv += this.HEADER;
    }

    append(mcngId: string, targetName: string, dateString: string):void {
        this.csv += '\n' + mcngId + '|' + targetName + '|' + dateString + '|' + 'mcng' + '|' + '3';
    }

    build(): string {
        return this.csv;
    }

    getFileName(): string {
        return 'P_TARGET_FLAT.csv';
    }
}
