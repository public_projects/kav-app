import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {AreaModel} from '../model/AreaModel';

@Injectable({
  providedIn: 'root'
})
export class AreaService {
  private areaModels: AreaModel[];
  private alreadyCalled = false;
  public areaModelSubject = new Subject<AreaModel[]>();

  constructor(private http: HttpClient) {
  }

  sortByAreaName(areaModels: AreaModel[]): AreaModel[] {
    const byName = areaModels;
    byName.sort((a, b) => {
      const x = parseInt(a.name.substring(4, a.name.length));
      const y = parseInt(b.name.substring(4, b.name.length));
      return x - y;
    });

    return byName;
  }

  getAreas(): Observable<AreaModel[]> {
    // Avoids fetching data always by storing it in 'areaModels'
    if (this.alreadyCalled) {
      return new Observable<AreaModel[]>(subscriber => {
        console.log('Returning existing areaModel');
        this.areaModels = this.sortByAreaName(this.areaModels.slice(0));
        subscriber.next(this.areaModels);
      });
    }

    this.alreadyCalled = true;

    return this.http
      .get<{ [key: string]: AreaModel }>('https://greenhorn-c7a65.firebaseio.com/dev_db_1/area.json')
      .pipe(
        map(responseData => {
          const areaArray: AreaModel[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              areaArray.push({...responseData[key], name: key});
            }
          }
          console.log('Made new call to area!');
          this.areaModels = areaArray;
          this.areaModels = this.sortByAreaName(this.areaModels.slice(0));
          this.areaModelSubject.next(this.areaModels);
          return this.areaModels;
        }));
  }

  saveArea(areaModel: AreaModel): void {
    const requestAreaModels: AreaModel[] = [];
    requestAreaModels.push(areaModel);
    this.http.post('http://localhost:8982/create/area',
      requestAreaModels)
      .subscribe(
        (val) => {
          console.log('POST call successful value returned in body',
            val);
          this.areaModels.push(areaModel);
          this.areaModelSubject.next(this.areaModels);
          return new Observable((observer) => {
            observer.next('Success!');
          });
        },
        response => {
          console.log('POST call in error', response);
          return new Observable((observer) => {
            observer.error(response);
          });
        },
        () => {
          console.log('The POST observable is now completed.');
        });
  }

  saveArea2(areaModel: AreaModel): Observable<any> {
    const requestAreaModels: AreaModel[] = [];
    requestAreaModels.push(areaModel);
    return this.http.post('http://localhost:8982/create/area', requestAreaModels)
      .pipe(map(value => {
          console.log('[value]: ', value);
          return value;
        }
      ));
  }
}
