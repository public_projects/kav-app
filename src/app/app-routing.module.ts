import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MsisdnCgiTableComponent} from './component/msisdn-cgi-table/msisdn-cgi-table.component';
import {CgiActivityFormArrayComponent} from './component/cgi-activity-form-array/cgi-activity-form-array.component';
import {GetTargetComponent} from './component/get-target/get-target.component';
import {CdrCreatorTableComponent} from './component/cdr-creator/cdr-creator-table/cdr-creator-table.component';
import {UploadMfiCdrComponent} from './component/upload-mfi-cdr/upload-mfi-cdr.component';
import {TargetComponent} from './component/target/target.component';
import {BatchDataGeneratorComponent} from './component/batch-data-generator/batch-data-generator.component';

const routes: Routes = [
  {
    path: 'msisdn-cgi',
    component: MsisdnCgiTableComponent
  }, {
    path: 'cgi-activity-form-array',
    component: CgiActivityFormArrayComponent
  }, {
    path: 'cdr-creator',
    component: GetTargetComponent
  }, {
    path: 'add-and-delete-something',
    component: CdrCreatorTableComponent
  }, {
    path: 'upload-mfi-cdr',
    component: UploadMfiCdrComponent
  }, {
    path: 'target',
    component: TargetComponent
  }, {
    path: 'batch-data-generator',
    component: BatchDataGeneratorComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
