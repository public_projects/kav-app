import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {CgiActivityService} from './service/cgi-activity.service';
import {MsisdnCgiTableComponent} from './component/msisdn-cgi-table/msisdn-cgi-table.component';
import {LayoutModule} from '@angular/cdk/layout';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MainNavComponent} from './component/main-nav/main-nav.component';
import {CgiActivityFormArrayComponent} from './component/cgi-activity-form-array/cgi-activity-form-array.component';
import {GetTargetComponent} from './component/get-target/get-target.component';
import {TargetService} from './service/target.service';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatNativeDateModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {CdrCreatorTableComponent} from './component/cdr-creator/cdr-creator-table/cdr-creator-table.component';
import {DialogBoxComponent} from './component/dialog-box/dialog-box.component';
import {DateTimePickerModule} from '@syncfusion/ej2-angular-calendars';
import {DatePipe} from '@angular/common';
import {GeneratorService} from './service/generator.service';
import {AddAreaComponent} from './component/add-area/add-area.component';
import {UploadMfiCdrComponent} from './component/upload-mfi-cdr/upload-mfi-cdr.component';
import {AreaService} from './service/area.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';
import {TargetComponent} from './component/target/target.component';
import {ShowPartyInfoComponent} from './component/show-party-info/show-party-info.component';
import {GenerateMcngConfigDialogComponent} from './component/generate-mcng-config-dialog/generate-mcng-config-dialog.component';
import {FaunaAdminClientService} from './service/fauna-admin-client.service';
import {MatIconModule} from '@angular/material/icon';
import {BatchDataGeneratorComponent} from './component/batch-data-generator/batch-data-generator.component';
import {PartyIdentitySelectionComponent} from './component/party-identity-selection/party-identity-selection.component';
import {MatRadioModule} from '@angular/material/radio';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

@NgModule({
  declarations: [
    AppComponent,
    MsisdnCgiTableComponent,
    MainNavComponent,
    CgiActivityFormArrayComponent,
    GetTargetComponent,
    CdrCreatorTableComponent,
    DialogBoxComponent,
    AddAreaComponent,
    UploadMfiCdrComponent,
    TargetComponent,
    ShowPartyInfoComponent,
    GenerateMcngConfigDialogComponent,
    BatchDataGeneratorComponent,
    PartyIdentitySelectionComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        MatCardModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatDialogModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatSortModule,
        MatToolbarModule,
        MatMenuModule,
        LayoutModule,
        MatButtonModule,
        MatSidenavModule,
        MatListModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule,
        DateTimePickerModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatIconModule,
        MatRadioModule,
        MatAutocompleteModule
    ],

  providers: [CgiActivityService, TargetService, DatePipe, GeneratorService, AreaService, FaunaAdminClientService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
